
# Integra��o Spring + Inertia.js + Vue 3

- Implementa��o com a finalidade de estudo / desenvolvimento de uma aplica��o Spring Boot que se comunica facilmente com componentes Vue 3, sem a necessidade de utilizar roteadores na aplica��o Vue 3.
- Dessa forma, pode-se observar o funcionamento do fluxo de requisi��es entre o Spring e o Vue 3, utilizando o Inertia.js.
- O Build da aplica��o utiliza Vite e estiliza��o com Tailwind CSS.
  - O Vite � um build tool que permite que o desenvolvimento seja feito com hot-reload, e de forma muito r�pida.
  - Ao realizar o build da aplica��o Vue 3, os arquivos est�ticos s�o copiados para a pasta /build/resources/main/static/dist do Spring Boot.
  - O Spring Boot � respons�vel por servir os arquivos est�ticos e realizar as requisi��es para o Vue 3.
  - O Inertia intercepta as requisi��es e as transforma em requisi��es XHR, que s�o tratadas pelo Spring Boot.
  - O Spring Boot retorna somente os dados necess�rios para a atualiza��o parcial da p�gina.

## Aplica��o Vue 3 rodando no Spring Boot:

### Funcionamento:
- A aplica��o Vue 3 � compilada (npm run build-w) e os arquivos est�ticos s�o copiados para a pasta /build/resources/main/static/dist do Spring Boot.
- N�s criamos as classes que utilizam os padr�es do Inertia.js para renderizar os componentes Vue 3.
- As requisi��es subsequentes s�o interceptadas pelo Inertia.js, transformadas em requisi��es XHR.
  - O XHR � uma api que permite que o navegador realize requisi��es HTTP sem a necessidade de recarregar a p�gina.
  - O Inertia realiza requisi��es XHR ao Spring, que retorna somente os dados necess�rios � atualiza��o parcial da p�gina.
  - Utilizando componentes <inertia-link> no Vue 3, as requisi��es s�o feitas usando XHR, e as p�ginas n�o s�o totalmente recarregadas.

### Exemplo de como chamar um componente Vue 3 diretamente em um Controller no Spring:

- Em uma aplica��o Spring, basta criar um controller que retorne um ModelAndView, e utilizar o m�todo est�tico Inertia.renderVueComponent().
- O m�todo Inertia.renderVueComponent() recebe como par�metros o nome do componente Vue 3 que ser� renderizado, e um objeto que ser� passado como par�metro para o componente Vue 3.
- O m�todo Inertia.withProps() recebe como par�metro um objeto que ser� passado como par�metro para o componente Vue 3.

#### Java:
```java
@GetMapping("/")
public ModelAndView showHome() {
   Property property = Property.builder()
          .name("Exemplo")
          .build();
    return Inertia.renderVueComponent("Home", withProps(property));
}
```
- Nesse exemplo, as requisi��es para a rorta "/" ser�o renderizadas em um componente Vue 3 chamado "Home". 
- O objeto "property" serve como um container que abrigar� os par�metros passados diretamente para o componente Vue 3.

#### Vue 3: 

- No componente Vue 3, esses par�metros pode ser acessado utilizando props:

```javascript
const props = defineProps({
  name: Object
})
```


## Tecnologias utilizadas:

- Spring Boot 3.1
- Vue.js 3
- Inertia.js
- Vite
- Tailwind CSS


## M�gica: Inertia.js

O Inertia substitui a camada de visualiza��o tradicional por componentes de p�gina JavaScript, permitindo o uso de frameworks frontend como React, Vue ou Svelte, enquanto se aproveita as funcionalidades de frameworks do lado do servidor como o Spring Boot ou Laravel.

Em vez de recarregar toda a p�gina, o Inertia utiliza uma biblioteca de roteamento do lado do cliente que intercepta cliques em links e faz visitas �s p�ginas atrav�s de requisi��es XHR. Isso evita o recarregamento completo da p�gina, substituindo apenas o componente necess�rio e atualizando o estado do hist�rico do navegador.

O resultado � uma experi�ncia de aplica��o de p�gina �nica mais fluida e eficiente, combinando as vantagens do desenvolvimento do lado do servidor com a interatividade e a din�mica das aplica��es do lado do cliente.

## Rodando localmente

Clone o projeto

```bash
  git clone https://gitlab.com/luisow748/spring-inertia-vite-vue3.git
```

Entre no diret�rio do projeto

```bash
  cd spring-inertia-vite-vue3
```


Instalar depend�ncias do frontend:

```bash
npm install
```

Build do frontend para desenvolvimento (com hot-reload):

```bash
npm run build-w
```

Rodar a aplica��o java:

```bash
./gradlew bootRun
```

* Opcional:
  Em seguida (para o gradle procurar mudan�as no c�digo e gerar novo build):
```bash
./gradlew assemble -t
```





## Documenta��o da API de teste

#### Home da aplica��o

```http
  GET /
```
O controller da aplica��o Spring chama o Componente Vue "Home.vue" e passa um par�metro "name" para demonstrar a passagem de informa��es entre as aplica��es.
No componente Vue 3, para acessar os dados passados pelo Spring, basta utilizar props na tag setup:

```javascript
<script setup>
const props = defineProps({
  name: Object
})

props.name
...
```

#### Link de exemplo para demonstrar o funcionamento da integra��o entre a aplica��o Spring e o Vue.

```http
  GET /about
```
- Ao clicar no link "about", o Inertia intercepta a requisi��o e faz uma requisi��o XHR para o Spring, que retorna somente os dados necess�rios para a atualiza��o parcial da p�gina.



## Refer�ncia

- [Spring Boot](https://spring.io/projects/spring-boot)
- [Vue.js ](https://vuejs.org/)
- [Inertia.js](https://inertiajs.com/)


## Autores

- [@luisow748](https://gitlab.com/luisow748)


## Contribuindo

Contribui��es s�o sempre bem-vindas!



