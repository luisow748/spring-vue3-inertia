

import {defineConfig} from 'vite'
import vue from "@vitejs/plugin-vue";
import path from "node:path";
import {fileURLToPath, URL} from "node:url";
import glob from "glob";

export default defineConfig({
    root: "src/main/resources/templates",

    build: {
        manifest: true,
        outDir: "../static/dist",
        rollupOptions: {
            input: Object.fromEntries([
                'src/main/js/index.js',
                'src/main/resources/templates/css/main.css',
                glob.sync('../../js/**/*.{vue,js,ts,jsx,tsx,html}')
                    .map((file) => `/${file})`)
                ]
            ),
            output: {
                format: "es",

                entryFileNames : `[name].js`,
                assetFileNames: `[name].[ext]`,
            }
        }
    },

    plugins: [

        // laravel({
        //     input: ["../../js/index.js", "./css/main.css"],
        //     refresh: true,
        // }),
        vue(),
    ],
    resolve: {
        alias: {
            "/src": path.resolve(process.cwd(), "src"),
            '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    }
})