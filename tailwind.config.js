/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [

        "./**/*.{vue,js,ts,jsx,tsx,html}",
        "../../js/**/*.{vue,js,ts,jsx,tsx,html}"
    ],
    theme: {
        extend: {
            colors: {
                primary: {
                    DEFAULT: "#4565dc",
                    muted: "#3b4b84",
                },
                secondary: {
                    DEFAULT: "#3c4258",
                    muted: "#575b68",
                },
                accent: {
                    DEFAULT: "#867e65",
                },
                dark: {
                    DEFAULT: "#252628",
                    200: "rgba(37,38,40,0.53)",
                    300: "rgba(37,38,40,0.72)",
                    400: "rgba(37,38,40,0.92)",

                },
                light: {
                    DEFAULT: "#cfcfcf",
                    muted: "#c0c0c0",
                }
            }
        },
    },
    darkMode: "class"
}

