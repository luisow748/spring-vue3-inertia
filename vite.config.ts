import {fileURLToPath, URL} from 'node:url'
import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import path from "node:path";

export default defineConfig({
    root: "src/main/resources/templates",
    // assetsInclude: ["/css/*.css"],
    build: {
        manifest: true,
        // outDir: "../../../../build/resources/main/static/dist",
        outDir: "../static/dist",
        rollupOptions: {
            input: {
                index: "src/main/js/index.js",

                css: "/css/main.css"
            },
            output: {
                entryFileNames : `[name].js`,
                assetFileNames: `[name].[ext]`,
            }
        }
    },

    plugins: [
        vue(),
    ],
    resolve: {
        alias: {
            "/src": path.resolve(process.cwd(), "src"),
            '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    }
})
