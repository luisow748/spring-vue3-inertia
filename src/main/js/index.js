import { createApp, h } from 'vue'
import { App, plugin } from '@inertiajs/inertia-vue3'

import { createInertiaApp, Head, Link } from '@inertiajs/inertia-vue3';


createInertiaApp({

    resolve: name => {
        const pages = import.meta.glob('./Pages/**/*.vue', { eager: true })
        return pages[`./Pages/${name}.vue`]
    },
    setup({ el, app, props, plugin }) {
        return createApp({ render: () => h(app, props) })
            .use(plugin)
            .component('InertiaHead', Head)
            .component('InertiaLink', Link)
            // .mixin({ methods: { route } })
            .mount(el);
    },
});

