

export async function loadDynamicCss() {
    const dynamicCssLinks = [];

    try {
        // Faz uma chamada ass�ncrona para obter a lista de arquivos CSS no diret�rio
        const modules = await import.meta.glob("/static/dist/*.css");

        // Itera sobre os m�dulos importados (arquivos CSS)
        for (const key in modules) {
            const module = modules[key];

            // Crie um elemento <link> para cada arquivo CSS
            const link = document.createElement('link');
            link.setAttribute('rel', 'stylesheet');
            link.setAttribute('type', 'text/css');
            link.setAttribute('href', module.default);

            // Anexe o elemento <link> ao <head> do documento
            document.head.appendChild(link);

            // Adicione a tag <link> � lista de links din�micos
            dynamicCssLinks.push(link);
        }
    } catch (error) {
        console.error('Erro ao importar os arquivos CSS dinamicamente', error);
    }

    return dynamicCssLinks;
}
