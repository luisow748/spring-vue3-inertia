package com.luisow748.springinertiajs.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Property {

    private String name;
}
