package com.luisow748.springinertiajs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringInertiaJsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringInertiaJsApplication.class, args);
	}

}
