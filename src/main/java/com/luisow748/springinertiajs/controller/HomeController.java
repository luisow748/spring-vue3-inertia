package com.luisow748.springinertiajs.controller;


import com.luisow748.springinertiajs.model.Property;
import inertia.spring.inertia.Inertia;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import static inertia.spring.inertia.Inertia.Props.withProps;


@Controller
@RequiredArgsConstructor
public class HomeController {

    @GetMapping("/")
    public ModelAndView showHome() {
        Property property = Property.builder()
                .name("Exemplo de parametro passado para o componente Vue")
                .build();
        return Inertia.renderVueComponent("Home", withProps(property));
    }

    @GetMapping("/about")
    public ModelAndView showTeste() {
        Property property = Property.builder().build();
        property.setName("About");
        return Inertia.renderVueComponent("About", withProps(property));
    }

}
